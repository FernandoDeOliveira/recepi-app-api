from django.contrib.auth import get_user_model
from django.test import TestCase

from core import models

def sample_user(email='user_test@eemail.com', password='his_pass'):
    """Create a sample user"""
    return get_user_model().objects.create_user(email, password)

class ModelTestes(TestCase):

    def test_create_user_with_email_successfull(self):
        """Test creating a new user with an email is successfull"""
        email = "test@email.com"
        password = "thispassword123"

        user = get_user_model().objects.create_user(
            email=email,
            password=password
        )

        self.assertEqual(user.email, email,
                         'email of user must be the same')
        self.assertTrue(user.check_password(password),
                        'password must be the same')

    def test_new_user_email_normalized(self):
        """Test the email for a new user is normalized"""
        email = "test@EMAIL.COM"
        user = get_user_model().objects.create_user(email, 'test123')

        self.assertEqual(user.email, email.lower(), 'email must be normalized')

    def test_new_user_invalid_email(self):
        """Test creating user with no email raiser error"""
        with self.assertRaises(ValueError):
            get_user_model().objects.create_user(None, 'test123')

    def test_create_new_superuser(self):
        """Test creating a new superuser"""
        user = get_user_model().objects.create_superuser(
            'test@email.com',
            'test123'
        )
        self.assertTrue(user.is_superuser, 'it must be a superuser')
        self.assertTrue(user.is_staff, 'it must be a staff')

    def test_tag_str(self):
        """Test the tag string representation"""
        tag = models.Tag.objects.create(
            user=sample_user(),
            name='meat'
        )

        self.assertEqual(str(tag), tag.name)